//CLASS FOR CREATE A 3D VISOR WHIT ALL HIS CONTENT
var view3D = function(sceneConfig) {
    var self = this;

    //Public Atrib
    self.camera = null;
    self.axialAxis = null;
    self.frontalAxis = null;
    self.scene = null;
    self.render = null;
    self.plane = null;
    self.config = null;
    self.addedPlanesArray = [];
    self.removedPlanesArray = [];
    
    //Private Atrib
    var planeTexture, lastTime, renderedView;
    var activeFilter = 0, delta;

    //INIT CALLS
    addScene(sceneConfig); //create the scene
    animateScene(); //Animation and render the scene
    
        /*#####################################################################  
    INFO: create the camera for the scene
        @config: The config of camera (with camera view and other things)
    ####################################################################*/
    function createCamera(config) {
        var camera = new THREE.OrthographicCamera(config.canvasWidth / - 200 ,
        config.canvasWidth / 200, config.canvasHeight / 200, config.canvasHeight / - 200, 1, 100);      
        switch(config.camera.type) {
            case "axial":
                camera.position.set(0,0, degToRad(90));    
                break;
            case "frontal":
                camera.position.set(degToRad(90),0,0);
                break;
            case "sagital":
                camera.position.set(0,degToRad(90),0);
                break;
            case "3D":
                camera = new THREE.OrthographicCamera(config.canvasWidth / - 100 ,
                config.canvasWidth / 100, config.canvasHeight / 100, config.canvasHeight / - 100, 1, 100);
                camera.position.set(6, degToRad(85), 6);
                break;
            default:
                alert("no tenemos camara para la vista: " + type);
                break;
        }
        camera.lookAt(self.scene.position);
        return camera;
    }
    /*#####################################################################  
    INFO: Create the model planes of the object 3D and adds it to the 
    scene
    ####################################################################*/
    function getPlanes() {
        var planeGeometry, planeMaterial;
        $.each(patients, function(index, patient) {
            if (patient.name === "Javier Prieto Diaz") { //This has to be selected by user
                $.each(patient.images, function(index, imgPath) {
                    planeTexture = new THREE.ImageUtils.loadTexture(imgPath);
                    //planeTexture.minFilter = THREE.NearestFilter;
                    //planeTexture.magFilter = THREE.NearestFilter;
                    planeMaterial = new THREE.MeshBasicMaterial({ map:planeTexture, side:THREE.DoubleSide });
                    planeGeometry = new THREE.CubeGeometry(self.config.planeGeometry.x, self.config.planeGeometry.y, self.config.planeGeometry.z);
                    self.plane = new THREE.Mesh(planeGeometry, planeMaterial);
                    self.plane.position.set(self.config.spaceBetweenPlanes.x, self.config.spaceBetweenPlanes.y, self.config.spaceBetweenPlanes.z);
                    self.config.spaceBetweenPlanes[self.config.axis] += 0.05;
                    self.addedPlanesArray.push(self.plane);
                    self.scene.add(self.plane); 
                });  
            }
        });
        self.removedPlanesArray.push(self.addedPlanesArray[self.addedPlanesArray.length - 1]);
    }
    /*#####################################################################  
    INFO: Get the Axis for cut the object planes
    ####################################################################*/
    function getAxis() {
        //AXIS PLANE TO CUT THE SCENE
        //decides what is viewable and what is not
        var axisGeometry = new THREE.CubeGeometry(5.0, 5.0, 0.0);

        //PLANOZ - axialAxis
        var axisMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: "red"});
        self.axialAxis = new THREE.Mesh(axisGeometry, axisMaterial);
        self.axialAxis.rotation.z = degToRad(90);
        self.scene.add(self.axialAxis);

        //PLANOY - frontalAxis
        var axisMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: "blue"});
        self.frontalAxis = new THREE.Mesh(axisGeometry, axisMaterial);
        self.frontalAxis.rotation.y = degToRad(90);
        self.scene.add(self.frontalAxis);

        //PLANOX - sagital axis
        var axisMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: "yellow"});
        self.sagitalAxis = new THREE.Mesh(axisGeometry, axisMaterial);
        self.sagitalAxis.rotation.x = degToRad(90);
        self.scene.add(self.sagitalAxis);
    }
    /*#####################################################################  
    INFO: addScene creates a complete new scene
        @conf: configuration of the scene. 
            If the config is an string named "default" the config
            will be the defaultConfig.
    ####################################################################*/
    function addScene(conf) {
        //Config by Default
        self.config = {
            renderBgColor : "#000000",
            canvasWidth : 519,
            canvasHeight : 275
        }

        if(self.config == "default") self.config = {};
        $.extend(self.config, conf);

        //Render
        self.render = new THREE.WebGLRenderer();
        self.render.setClearColor(self.config.renderBgColor, 1); //Render Background Color
        self.render.setSize(self.config.canvasWidth, self.config.canvasHeight); //canvas Size
        
        //Add to the HTML
        var box = $('<div class="box canvasCont">')
            .append($('<div class="box-title">').text(self.config.name))
            .append(self.render.domElement);
        $("#workspace").append(box);

        self.scene = new THREE.Scene();//Scene
        getPlanes();//Planes
        getAxis();//Axis

        //Camera
        self.camera = createCamera(self.config);
        self.scene.add(self.camera);
    }   
    /*#####################################################################  
    INFO: renderScene renders the scene
    ####################################################################*/
    function renderScene(){
        self.render.render(self.scene, self.camera);
    }
    /*#####################################################################  
    INFO: animateScene adds the animation wheel and calls to render
    ####################################################################*/
    function animateScene() {
        delta = (Date.now()-lastTime)/1000;
        if (delta > 0) renderScene();
        lastTime = Date.now();
        requestAnimationFrame(animateScene);
    }   
    
    //METHODS
    /*#####################################################################  
    INFO: Returns the html dom element
    ####################################################################*/    
    self.getDomElement = function () {
        return self.render.domElement;
    }
    /*#####################################################################  
    INFO: Show or destroy planes the user sees if makes scroll
    ####################################################################*/ 
    self.showPlane = function() {
        // console.log("Added: ");
        // console.log(self.addedPlanesArray);
        // console.log("Removed: ");
        // console.log(self.removedPlanesArray);
        var addedArrayLength = self.addedPlanesArray.length - 1;
        var removedArrayLength = self.removedPlanesArray.length - 1;
        //Remove plane
        if ((addedArrayLength > 0) && (self.addedPlanesArray[[addedArrayLength]].position.z > self.axialAxis.position.z)) {
            self.scene.remove(self.addedPlanesArray[addedArrayLength]);
            self.removedPlanesArray.push(self.addedPlanesArray[addedArrayLength]);
            self.addedPlanesArray.pop();
        }
        //Add plane
        if ((removedArrayLength > 0) && 
            (self.removedPlanesArray[[removedArrayLength]].position.z < self.axialAxis.position.z)) {
            self.scene.add(self.removedPlanesArray[removedArrayLength]);
            self.addedPlanesArray.push(self.removedPlanesArray[removedArrayLength]);
            self.removedPlanesArray.pop();
        }
    }
}

//INIT VIEWS
var webGLInit = function() {
    lastTime=Date.now();

    var view, viewsArray = [];
    $.each(viewsConfig, function(index, viewConfig) {
        view = new view3D(viewConfig);
        viewsArray.push(view);
    });

    $.each(viewsArray, function(index, view){
        // if (view.config.name == "3D") { //Rotation for 3D view 
            // $(view.getDomElement()).onmouseover(function() {
            //     console.log("hola");
            // });
        // }
        // else { // Movement with mouse
            $(view.getDomElement()).mousewheel(function(objEvent, intDelta){
                var delta = (Date.now()-lastTime)/1000;
                view.showPlane();
                if (intDelta > 0) {
                    view.axialAxis.position[view.config.axis] -= 0.1;
                    view.camera.position[view.config.axis] -= 0.1;
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) viewsArray[i][view.config.name].position[view.config.axis] -= 0.1;
                    }
                }
                else if (intDelta < 0) {
                    view.axialAxis.position[view.config.axis] += 0.1;
                    view.camera.position[view.config.axis] += 0.1;
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) viewsArray[i][view.config.name].position[view.config.axis] += 0.1;
                    }
                }    
            });
        // }
        //Click for Maximize the view
        $(view.getDomElement()).click(function (){
            if (!view.config.Contsize) view.config.Contsize = 'small';
            switch(view.config.Contsize) {
                case 'small':
                    $(this).parent().css('height', '590').css('width', '1000');
                    view.render.setSize(1000, 590);
                    view.config.Contsize = 'medium';
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) $(viewsArray[i].getDomElement()).parent().css("display", "none");
                    }
                break;
                case 'medium':
                    $(this).parent().css('width', '519').css('height', '280');
                    view.render.setSize(519, 280);
                    view.config.Contsize = 'small';
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) $(viewsArray[i].getDomElement()).parent().css("display", "block");
                    }
                break;
                default:
                    alert("problema con tamaño caja");
                break;
            }
        });
    });
}