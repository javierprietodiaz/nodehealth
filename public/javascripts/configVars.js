//The ·3D Views configuration
var viewsConfig = [
    {
        axis:"z", name:"axialAxis", 
        planeGeometry : {x : 2.5, y : 2.5, z : 0.01},
        spaceBetweenPlanes : {x : 0.0, y : 0.0, z : -1.0},
        camera:{type:"axial"}
    },
    {
        axis:"x", name:"frontalAxis",
        planeGeometry: {x : 0.01, y : 2.5, z : 2.5},
        spaceBetweenPlanes : {x : -1.0, y : 0.0, z : 0.0},
        camera:{type:"frontal"}
    },
    {
        axis:"y", name:"sagitalAxis",
        planeGeometry: {x : 2.5, y : 0.01, z : 2.5},
        spaceBetweenPlanes : {x : 0.0, y : -1.0, z : 0.0},
        camera:{type:"sagital"}
    },
    {
        axis:"3D", name:"3D",
        planeGeometry : {x : 2.5, y : 2.5, z : 0.01},
        spaceBetweenPlanes : {x : 0.0, y : 0.0, z : -1.0},
        camera:{type:"3D"}
    }
];

//FAKE patients JSON (Delete when API)
var patients = [
    {
        name : "Javier Prieto Diaz",
        dni: "20475156G",
        images: [
            "images/20475156G/textura0.png",
            "images/20475156G/textura1.png",
            "images/20475156G/textura2.png",
            "images/20475156G/textura3.png",
            "images/20475156G/textura4.png",
            "images/20475156G/textura5.png",
            "images/20475156G/textura6.png",
            "images/20475156G/textura7.png",
            "images/20475156G/textura8.png",
            "images/20475156G/textura9.png",
            "images/20475156G/textura10.png",
            "images/20475156G/textura11.png",
            "images/20475156G/textura12.png",
            "images/20475156G/textura13.png",
            "images/20475156G/textura14.png",
            "images/20475156G/textura15.png",
            "images/20475156G/textura16.png",
            "images/20475156G/textura17.png",
            "images/20475156G/textura18.png",
            "images/20475156G/textura19.png"
        ]
    },
    {
        name : "Jonathan Ruiz Peinado",
        dni: "12345678G",
        images: [
            "images/12345678G/textura0.png",
            "images/12345678G/textura1.png",
            "images/12345678G/textura2.png",
            "images/12345678G/textura3.png",
            "images/12345678G/textura4.png",
            "images/12345678G/textura5.png",
            "images/12345678G/textura6.png",
            "images/12345678G/textura7.png",
            "images/12345678G/textura8.png",
            "images/12345678G/textura9.png",
            "images/12345678G/textura10.png",
            "images/12345678G/textura11.png",
            "images/12345678G/textura12.png",
            "images/12345678G/textura13.png",
            "images/12345678G/textura14.png",
            "images/12345678G/textura15.png",
            "images/12345678G/textura16.png",
            "images/12345678G/textura17.png",
            "images/12345678G/textura18.png",
            "images/12345678G/textura19.png"
        ]
    }
];