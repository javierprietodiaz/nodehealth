//INIT VIEWS
var webGLInit = function() {
    lastTime=Date.now();

    var view, viewsArray = [];
    $.each(viewsConfig, function(index, viewConfig) {
        view = new view3D(viewConfig);
        viewsArray.push(view);
    });

    $.each(viewsArray, function(index, view){
        // if (view.config.name == "3D") { //Rotation for 3D view 
            // $(view.getDomElement()).onmouseover(function() {
            //     console.log("hola");
            // });
        // }
        // else { // Movement with mouse
            $(view.getDomElement()).mousewheel(function(objEvent, intDelta){
                var delta = (Date.now()-lastTime)/1000;
                view.showPlane();
                if (intDelta > 0) {
                    view.axialAxis.position[view.config.axis] -= 0.1;
                    view.camera.position[view.config.axis] -= 0.1;
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) viewsArray[i][view.config.name].position[view.config.axis] -= 0.1;
                    }
                }
                else if (intDelta < 0) {
                    view.axialAxis.position[view.config.axis] += 0.1;
                    view.camera.position[view.config.axis] += 0.1;
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) viewsArray[i][view.config.name].position[view.config.axis] += 0.1;
                    }
                }    
            });
        // }
        //Click for Maximize the view
        $(view.getDomElement()).click(function (){
            if (!view.config.Contsize) view.config.Contsize = 'small';
            switch(view.config.Contsize) {
                case 'small':
                    $(this).parent().css('height', '590').css('width', '1000');
                    view.render.setSize(1000, 590);
                    view.config.Contsize = 'medium';
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) $(viewsArray[i].getDomElement()).parent().css("display", "none");
                    }
                break;
                case 'medium':
                    $(this).parent().css('width', '519').css('height', '280');
                    view.render.setSize(519, 280);
                    view.config.Contsize = 'small';
                    for(var i = 0; i < viewsArray.length; i++) {
                        if (index !== i) $(viewsArray[i].getDomElement()).parent().css("display", "block");
                    }
                break;
                default:
                    alert("problema con tamaño caja");
                break;
            }
        });
    });
}