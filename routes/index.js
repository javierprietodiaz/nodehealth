// Creación de la Conexión
var mongoose        = require('mongoose')
  , db_lnk          = 'mongodb://localhost/nodeHealth'
  , db              = mongoose.createConnection(db_lnk)

// Creación de variables para cargar el modelo
var patients_schema = require('../models/patient')
  , Patients = db.model('patients', patients_schema) //el 'patients' es la collection dl mongo


/*
 * GET home page.
 */

exports.index = function (req, res, next) {

  Patients.find(gotPatients)

  // NOTA: Creo que es bueno usar verbos en inglés para las funciones,
  // por lo cómodo que son en estos casos (get, got; find, found)
  function gotPatients (err, patients) {
    if (err) {
      console.log(err)
      return next()
    }
    return res.render('index', {title: 'Lista de Productos', patients: patients})
  }
}